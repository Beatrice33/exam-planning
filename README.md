# Exam scheduling process
This project, which is also my bachelor thesis application is intended to help the teachers of Transylvania University of Brasov. It represents the digitalisation of the exam scheduling process, reducing the long time spent for the internal student-teacher-secretarial flow (minutes instead of weeks). 

It plays a significant role in avoiding possible human mistakes that can occur in classroom overlap for exams.

## Functionalities
On the Intranet platform the teachers have access to a module where
they can see all their subjects taught from the current semester. They can
set the date and time for their exams and they have at their disposal
useful information: the number of students enrolled in a class, the
number of students who retake the exam and the available places in a
classroom. All the subjects are grouped by faculty because the same course can be taught at more than one faculty.

![](src/Images/2.PNG)


The access to this page is conditioned by the academic period. The Secretariat sets a time frame in which the page can be used by the teachers. After this period of time the page can be still used in read-only mode for the teachers to have an overview of the following exams.

![](src/Images/4.PNG)
_The blue "Edit" icon for every row is disabled, so the users can't modify any information._

Every row represents a subject and includes the most relevant information for it.

Some of the subjects have several groups of students enrolled which can be viewed after expanding the row using the right arrow.
![](src/Images/3.PNG)

To access the edit mode for one of the subjects, the users have to press the blue icon. They have to choose the type of the exam (oral or written), the date and the classroom. After this selection, a calendar with the exams already scheduled by other teachers for that specific classroom and date and will be displayed:
![](src/Images/6.PNG)
_They can choose a timeslot by clicking the calendar or using the inputs below

![](src/Images/7.PNG)
_Confirmation of the correct information

In backend a set of verification will be made and 3 possible warning messages will appear:
- if the teacher has another exam scheduled at the same time
  
- if the classroom doesn't have enough places available left

- if the students from the selected major have another exam scheduled in the same day

  
The status of an exam it's highlighted using warning messages inside the table("Incomplet"-"Incomplete, "Necompletat"-"Unscheduled" or "Programat cu mai multe date"-"Scheduled with different dates").


By default the page contains information about teachers exams, but using the yellow button from the footer, a list with the scheduled exams where the user have to participate as an assistant will be displayed.
![](src/Images/9.PNG)


## Installation

Install dependencies
```bash
  npm install
```
Run the app
```bash
  npm start
```
