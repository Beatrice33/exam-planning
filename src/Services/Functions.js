import axios from "../axios-AGSIS-API";

const getAnUnivCurent = async () => {
    return await
        axios
            .get("AnUniversitar/AnUniversitarCurent")
            .then(response => {
                return response.data
            })
            .catch(error => {
                console.log(error)
            })
}

const getIdProfesor= async (username)=> {
    return await
        axios
            .get(`Profesor/ProfesorGetByUsernameSimplu?Username=${username}`)
            .then(response => {
                return response.data
            })
            .catch(error => {
                console.log(error)
            })
}


const getSesiuneExameneSpecializare = async(ID_Profesor, ID_AnUniv) => {
    return await
        axios
            .get(`ProgramareExamene/SesiuneExameneSpecializareListByProfesorAnUniv?ID_Profesor=${ID_Profesor}&ID_AnUniv=${ID_AnUniv}`)
            .then(response => {
                return response.data
            })
            .catch(error => {
                console.log(error);
            })
}

const getSesiuniActive = async(ID_AnUniv, ID_Profesor) => {
    return await
        axios
            .get(`ProgramareExamene/SesiuneExameneListByProfesorAnUniv?ID_AnUniv=${ID_AnUniv}&ID_Profesor=${ID_Profesor}`)
            .then(response => {
                return response.data
            })
            .catch(error => {
                console.log(error);
                alert('A aparut o eroare la incarcarea datelor')
            })
}
const getExameneProgramateProfesor =async(ID_AnUniv, username)=> {
    return await
        axios
            .get(`ProgramareExamene/ProgramareExamenProfesorListByAnUnivProfesor?ID_AnUniv=${ID_AnUniv}&ID_Profesor=${username}&Activa=1&ID_ProgramareExamen=-1`)
            .then(response => {
                return response.data
            })
            .catch(error => {
                console.log(error);
            })
}

const getMateriiProfesor =async(ID_AnUniv, username,ID_Semestru)=> {
    return await
        axios
            .get(`ProgramareExamene/DetaliuPlanSemestruListByProfesorSemestru?ID_AnUniv=${ID_AnUniv}&ID_ProfesorTitularCurs=${username}&ID_SemestruDinAn=${ID_Semestru}`)
            .then(response => {
                return response.data
            })
            .catch(error => {
                console.log(error);
            })
}


const existaExamenAceeasiZiOraProfesor=async(ID_AnUniv, ID_SesiuneExamene, ID_ProfesorTitular, dataDeVerificat, ID_Specializare, ID_AnStud,ID_Materie)=>{
    return await
        axios
            .get(`ProgramareExamene/ProgramareExamenProfesorMaiAreLaAceeasiZiOra?ID_AnUniv=${ID_AnUniv}&ID_SesiuneExamene=${ID_SesiuneExamene}
            &ID_ProfesorTitularCurs=${ID_ProfesorTitular}&dataOraDeVerificat=${dataDeVerificat}&ID_Specializare=${ID_Specializare}&ID_AnStud=${ID_AnStud}&ID_Materie=${ID_Materie}`)
            .then(response => {
                return response.data
            })
            .catch(error => {
                console.log(error);
            })
}

const existaExamenAceeasiZiSpecializare=async(ID_AnUniv, ID_SesiuneExamene, ID_Specializare, ID_Materie, dataOraDeVerificat,ID_TipFormaInv, NumarZile)=>{
    return await
        axios
            .get(`ProgramareExamene/ProgramareExamenProfesorSpecializareaMaiAreLaAceeasiZi?ID_AnUniv=${ID_AnUniv}&ID_SesiuneExamene=${ID_SesiuneExamene}&ID_Specializare=${ID_Specializare}&ID_Materie=${ID_Materie}&dataOraDeVerificat=${dataOraDeVerificat}&ID_TipFormaInv=${ID_TipFormaInv}&NumarZile=${NumarZile}`)
            .then(response => {
                return response.data
            })
            .catch(error => {
                console.log(error);
            })
}

const programareExamenMerge=async (dateProgramareExamen)=>{
    return await
        axios
            .post(`ProgramareExamene/ProgramareExamenMerge`, dateProgramareExamen)
            .then(result => {
                return result.status.toString().substring(0, 1)
            })
            .catch(error => {
                console.log(error.response.data)
            })
}

const getSaliByID_Facultate=async (ID_Facultate)=>{
    return await
        axios
            .get(`ProgramareExamene/SalaListGetByID_Facultate?ID_Facultate=${ID_Facultate}`)
            .then(result => {
                return result.data
            })
            .catch(error => {
                alert('A aparut o eroare' + error);
            })
}

const salaEsteOcupata=async (ID_AnUniv, Data, ID_Sala, DurataExamen, ID_Specializare, ID_AnStud, ID_Materie)=>{
        return await
            axios
                .get(`ProgramareExamene/ProgramareExamenProfesorSalaEsteOcupata?ID_AnUniv=${ID_AnUniv}&Data=${Data}&ID_Sala=${ID_Sala}&DurataExamen=${DurataExamen}&ID_Specializare=${ID_Specializare}&ID_AnStud=${ID_AnStud}&ID_Materie=${ID_Materie}`)
                .then(result => {
                    return result.data
                })
                .catch(error => {
                    console.log (error)
                })

}


const orarSali=async(Data,ID_Sala)=>{
    return await
    axios
        .get(`ProgramareExamene/ProgramareExameneDisponibilitateSali?Data=${Data}&ID_Sala=${ID_Sala}`)
        .then(result=>{
            return result.data
        })
        .catch(error=>{
            console.log(error)
        })
}

const setariUniversitare=async()=>{
    return await
        axios
            .get(`ProgramareExamene/SetariProgramareExamen`)
            .then(result=>{
                return result.data
            })
            .catch(error=>{
                console.log(error)
            })
}


export{
    getAnUnivCurent,
    getSesiuneExameneSpecializare,
    getExameneProgramateProfesor,
    getMateriiProfesor,
    getSesiuniActive,
    existaExamenAceeasiZiOraProfesor,
    existaExamenAceeasiZiSpecializare,
    programareExamenMerge,
    getIdProfesor,
    getSaliByID_Facultate,
    salaEsteOcupata,
    orarSali,
    setariUniversitare
}