import { Calendar, momentLocalizer } from 'react-big-calendar'
import moment from 'moment'
import 'moment/locale/ro'
import "../Design/CalendarSali.css"
import  'react-big-calendar/lib/css/react-big-calendar.css'
import PropTypes from "prop-types";
import {Message} from "semantic-ui-react";

const localizer = momentLocalizer(moment)


const CalendarSaliComponent = props => {
    if (props.exameneOrar && props.exameneOrar.length>0)
    {
    return (
        <Calendar
            eventPropGetter={(props.eventStyleGetter)}
            startAccessor='start'
            endAccessor='end'
            localizer={localizer}
            step={15}        // duration of the slot
            timeslots={4}
            events={props.exameneOrar}
            defaultView={'day'}
            style={{height: 500, zIndex:"inherit"}}
            selectable={true}
            defaultDate={new Date(props.dataSelectata.getFullYear(), props.dataSelectata.getMonth(), props.dataSelectata.getDate())}
            onSelectSlot={props.handleSelectTimeSlot}
            // dayLayoutAlgorithm="no-overlap"
            dayLayoutAlgorithm="overlap"
             min={new Date(props.dataSelectata.getFullYear(), props.dataSelectata.getMonth(), props.dataSelectata.getDate(), 8, 0, 0)}
            max={new Date(props.dataSelectata.getFullYear(), props.dataSelectata.getMonth(), props.dataSelectata.getDate(), 21, 0, 0)}
        />
    )}
    else {
        console.log(props.exameneOrar)
        return (
            <div style={{paddingTop:"20px", paddingBottom:"20px"}}>
                <Message
                    info
                    header='Sala liberă'
                    content='Această sală este liberă pe tot parcursul zilei selectate'
                />
            </div>
        )
    }
}

CalendarSaliComponent.propTypes = {
    dataSelectata:PropTypes.any,
    exameneOrar:PropTypes.array,
    handleSelectTimeSlot:PropTypes.func.isRequired,
    eventStyleGetter:PropTypes.func.isRequired
}
export default CalendarSaliComponent