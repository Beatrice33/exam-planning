import PropTypes from "prop-types";
import React from "react";
import {Icon, Table, Popup, Dropdown} from 'semantic-ui-react';
import 'rc-time-picker/assets/index.css';
import Sigla from "./Sigla";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";
import "../Design/ProgramareExamene.css"
import DatePicker from "react-datepicker";




const TableRowComponent = (props) => {
    return (
        <Table.Row
            active={!props.esteTableAsistentCurs  && ((!props.esteTableRowSpecializare && ( !props.disableRow || (props.activeRow===props.index && props.element.isExpanded))) || (props.esteTableRowSpecializare && props.activeRow===props.index && !props.element.isExpanded))}
            disabled={!props.esteTableAsistentCurs &&((props.element.examenProgramatDeSecretariat  || !props.element.examenProgramatDeSecretariat) && props.disableRow) && ((props.activeRow !== props.index && props.disableRow) || (props.semestruDinAn.toString()!==props.element.NumarSemestruDinAn) )}>


            <Table.Cell style={{width: "80px", backgroundColor:!props.afisareMateriiTitular&& "#efc75f"}}>
                {props.element.examenProgramatDeSecretariat? <strong style={{color:'green'}}>Programat Secretariat</strong> :
                !props.esteTableAsistentCurs?
                props.esteTableRowSpecializare &&
                    <>
                     <Popup content={(props.sesiuneExamenSelectata!==null && props.sesiuneExamenSelectata.semaipotprogramaexamene!==1)?'Nu se mai pot modifica datele pentru acest examen':'Setați datele acestui examen'}
                      trigger={<Icon
                            disabled={(props.activeRow !== props.index && props.disableRow)||(props.semestruDinAn.toString()!==props.element.NumarSemestruDinAn)|| (props.sesiuneExamenSelectata!==null && props.sesiuneExamenSelectata.semaipotprogramaexamene!==1)}
                            style={{transform: "scale(1.3)", marginRight: "20px"}}
                            name={'edit'} color={'blue'}
                            index={props.index}
                            onClick={props.handleRowDisabled}/>}/>

                        {props.element.Grupe.length > 1 &&
                            <Icon
                                disabled={(props.activeRow !== props.index && props.disableRow) || (props.semestruDinAn.toString()!==props.element.NumarSemestruDinAn) }
                                style={{transform: "scale(1.4)"}}
                                name={(props.element.isExpanded) ? 'caret down' : 'caret right'}
                                color={(props.element.isExpanded) ? 'black' : 'grey'}
                                index={props.index}
                                onClick={props.handleExpandedRows}/> }
                    </>
                    :<strong>{props.element.DenumireTipProfesorAsociere}</strong>
                }
            </Table.Cell>
            <Table.Cell
                // style={{maxWidth:"200px"}}
                disabled={props.element.isExpanded && props.activeRow===props.index && props.esteTableRowSpecializare}>{props.element.Denumire}
                {props.element.examenProgramatDeSecretariat? null:
                !props.esteTableAsistentCurs &&
                    <>
                        <Popup content={'Numarul stundeților înscriși la curs'}
                               trigger={
                    <Icon name={'group'} style={{margin: "5px", color: "#4353a1"}}/>}/>
                        {
                            props.esteTableRowSpecializare?
                            <strong>{props.element.NrStudentiGrupa}</strong>
                                :<strong>{props.subelement.NrStudentiGrupa}</strong>
                        }
                        {props.esteTableRowSpecializare &&

                        props.element.NrStudentiRestantieri>0 &&
                            <>
                       <Popup content={'Numarul stundeților restanțieri'}
                                       trigger={
                        <Icon name={'group'} color='red'  style={{margin: "5px"}}/>}/>
                        <strong>{props.element.NrStudentiRestantieri}</strong>
                            </>
                        }
                    </>
                }
            </Table.Cell>
            <Table.Cell
                disabled={props.element.isExpanded && props.activeRow===props.index && props.esteTableRowSpecializare}
            >
                <Sigla
                    icon={props.element.ID_Facultate}
                    size={"large"}
                    style={{marginRight:"5px"}}
                />
                <strong>{props.element.DenumireScurtaFacultate}</strong>
            </Table.Cell>
            <Table.Cell disabled={props.element.isExpanded && props.activeRow===props.index && props.esteTableRowSpecializare}>
                {props.element.DenumireSpecializare}
                {/*// abbreviations for all type of faculties*/}
                <strong>
                    {props.element.ID_TipFormaInv === 1 && "  (Zi)"}
                    {props.element.ID_TipFormaInv === 2 && "  (IFR)"}
                    {props.element.ID_TipFormaInv === 3 && "  (ID)"}
                </strong>
            </Table.Cell>
            <Table.Cell>{props.element.DenumireAnStudiu}</Table.Cell>
            <Table.Cell disabled={props.element.isExpanded && props.activeRow===props.index && props.esteTableRowSpecializare}
            >{(props.esteTableAsistentCurs || props.element.examenProgramatDeSecretariat) ?
                props.element.DenumireGrupa:
                    props.esteTableRowSpecializare ? props.element.Grupe.length > 1 ?
                        "Toate grupele" : props.element.Grupe[0].Nume : props.subelement.Nume
            }
            </Table.Cell>


            {props.semestruDinAn.toString()===props.element.NumarSemestruDinAn.toString() &&
            <Table.Cell disabled={!props.disableRow} style={{width: "170px"}}>
                {(props.element.examenProgramatDeSecretariat || props.esteTableAsistentCurs)?
                    <strong style={{color: "gray"}}>
                        {(props.element.ID_TipExaminare === 1) && "Scris"}
                        {(props.element.ID_TipExaminare === 2) && "Oral"}
                    </strong> :
                (props.disableRow !== true && props.activeRow !== props.index)||(props.disableRow===true && props.activeRow!==props.index)?
                    props.element.ID_TipExaminare!==null &&
                    <strong style={{color: "gray"}}>
                        {(props.element.ID_TipExaminare === 1) && "Scris"}
                        {(props.element.ID_TipExaminare === 2) && "Oral"}
                    </strong> :
                    <Dropdown
                        search selection
                        disabled={props.element.isExpanded && props.activeRow===props.index && props.esteTableRowSpecializare}
                        placeholder='Tip examinare'
                        options={props.listaTipExaminare}
                        value={props.element.ID_TipExaminare}
                        onChange={(e,tipExaminare) => props.handleTipExaminare(e,tipExaminare, props.element)}/>
                }
            </Table.Cell>
            }

            {props.semestruDinAn.toString() === props.element.NumarSemestruDinAn.toString() &&
            <Table.Cell
                style={{width: "190px"}}>
                {props.esteTableAsistentCurs?
                    <strong style={{color: "green"}}>{moment(props.element.DataProgramareExamen).format('DD-MM-YYYY HH:mm')}</strong>:

                    props.activeRow !== props.index?
                    ((props.esteTableRowSpecializare || props.element.examenProgramatDeSecretariat)?

                        (props.element.DataProgramareExamen !== null ?
                            <strong style={{color: "gray"}}><Icon name='checkmark' color={'green'}/>
                                {moment(props.element.DataProgramareExamen).format('DD-MM-YYYY HH:mm')}
                            </strong>
                            : props.element.Grupe.every(grupa => grupa.DataProgramareExamen === null) ?
                                <strong style={{color: "gray"}}>
                                    <Icon name='exclamation' color={'red'}/>Necompletat
                                </strong>:
                                props.element.Grupe.find(grupa=>grupa.DataProgramareExamen===null)?
                                // in case an exam does not have the assigned date (at the beginning of the appointments)
                                <strong style={{color: "gray"}}>
                                    <Icon name='exclamation' color={'yellow'}/>Incomplet
                                </strong> :
                                <strong style={{color: "gray"}}>
                                    <Icon name='checkmark' color={'green'}/>Programat (date diferite)
                                </strong>)
                        : (props.subelement.DataProgramareExamen !== null ?
                            <strong style={{color: "gray"}}>{moment(props.subelement.DataProgramareExamen).format('DD-MM-YYYY HH:mm')}</strong>
                            : null)) :
                    <DatePicker
                    dateFormat="dd/MM/yyyy"
                    filterDate={props.isWeekday}
                    minDate={new Date(props.sesiuneExamenSelectata.datainceputpefacultate)}
                    maxDate={new Date(props.sesiuneExamenSelectata.datasfarsitpefacultate)}
                    disabled={props.element.isExpanded && props.activeRow===props.index && props.esteTableRowSpecializare}
                    selected={props.esteTableRowSpecializare ? props.element.DataProgramareExamen!==null && new Date(props.element.DataProgramareExamen): props.subelement.DataProgramareExamen!==null && new Date(props.subelement.DataProgramareExamen)}
                    onChange={(e)=>props.handleChangeDate(e, props.element, props.esteTableRowSpecializare ? null : props.subelement)}
                    />

                }
            </Table.Cell>
            }
            <Table.Cell style={{width:"130px"}}
                        disabled={props.element.isExpanded && props.activeRow===props.index && props.esteTableRowSpecializare}>
                {props.activeRow !== props.index?
                    (props.esteTableRowSpecializare || props.esteTableAsistentCurs) ?
                        <strong style={{color:'gray'}}>{props.element.DurataExamen}</strong> : <strong style={{color:'gray'}}>{props.subelement.DurataExamen}</strong>:
                null}
            </Table.Cell>
            <Table.Cell style={{width: "140px"}} >
                {props.activeRow !== props.index?
                    props.esteTableRowSpecializare || props.esteTableAsistentCurs ?
                        <strong style={{color:"gray"}}>{props.element.examenProgramatDeSecretariat?props.element.Sala: props.element.Sala && props.element.Sala.length>=1 && props.element.Sala.map(salaSelectata=>salaSelectata.text + " ")}</strong> : <strong style={{color:"gray"}}>{ props.subelement.Sala!==null && props.subelement.Sala.text}</strong>:
                    <Dropdown
                        search selection
                        disabled={props.element.isExpanded && props.activeRow===props.index && props.esteTableRowSpecializare}
                        value={props.esteTableRowGrupa? props.subelement.Sala && props.subelement.Sala.value: props.esteTableRowSpecializare && props.element.Sala && props.element.Sala.length===1 && props.element.Sala[0].value}
                        options={props.listaSaliFacultate}
                        onChange={(e,data) => props.handleChangeSala(e, data, props.element,props.esteTableRowSpecializare ? null : props.subelement)}
                        placeholder='Sala'
                    />
                     }

            </Table.Cell>
            <Table.Cell>

                {props.activeRow === props.index && props.disableRow &&
                <Popup
                    disabled={props.element.isExpanded && props.activeRow===props.index && props.esteTableRowSpecializare}
                    content={props.esteTableRowSpecializare? 'Programați examenul pentru toate grupele' : 'Programați examenul doar pentru această grupă'}
                       trigger={
                           <Icon disabled={(props.element.isExpanded && props.activeRow===props.index && props.esteTableRowSpecializare) || (props.esteTableRowSpecializare? (props.element.DataProgramareExamen===null || props.element.Sala.length!==1 || !props.element.ID_TipExaminare):(props.subelement.DataProgramareExamen===null || props.subelement.Sala===null || !props.element.ID_TipExaminare))}
                                 name={'check circle'} color={'green'} style={{transform: "scale(1.4)"}}
                                 onClick={()=>props.afisareOrarSali(props.element,props.esteTableRowSpecializare?null:props.subelement)}/>
                                 // onClick={() => props.programeazaExamen(props.element,props.esteTableRowSpecializare?null:props.subelement)}/>
                       }
                />
                }
            </Table.Cell>
            <Table.Cell>
                {props.activeRow === props.index && props.disableRow && props.esteTableRowSpecializare &&
                <Popup content='Renunțați la modificările făcute pentru acest examen'
                       trigger={<Icon name={'undo'} color={'blue'} style={{transform: "scale(1.4)"}}
                        onClick={() => props.cancelDisabledRows(props.element,props.subelement)}/> }/>
                }</Table.Cell>
        </Table.Row>
    )
}


TableRowComponent.propTypes = {
    index: PropTypes.number.isRequired,
    activeRow: PropTypes.number.isRequired,
    disableRow: PropTypes.bool.isRequired,
    element: PropTypes.object.isRequired,
    handleRowDisabled: PropTypes.func,
    cancelDisabledRows: PropTypes.func,
    programeazaExamen: PropTypes.func,
    handleChangeDate: PropTypes.func,
    handleExpandedRows: PropTypes.func,
    listaTipExaminare: PropTypes.array,
    listaTipExamen: PropTypes.array,
    esteTableRowSpecializare: PropTypes.bool.isRequired,
    esteTableRowGrupa:PropTypes.bool.isRequired,
    subelement: PropTypes.object,
    handleTipExaminare:PropTypes.func,
    semestruDinAn:PropTypes.number.isRequired,
    sesiuneExamenSelectata: PropTypes.object,
    esteTableAsistentCurs:PropTypes.bool,
    isOpen:PropTypes.bool,
    handleClose:PropTypes.bool,
    handleOpen:PropTypes.bool,
    timeoutLength:PropTypes.number,
    afisareMateriiTitular:PropTypes.bool.isRequired,
    listaTipDurataExamen: PropTypes.array,
    listaSaliFacultate:PropTypes.array,
    handleChangeSala:PropTypes.func,
    afisareOrarSali:PropTypes.func,
    salaSelectataTabel: PropTypes.object

}

export default TableRowComponent;