import React, {Component} from "react";
import {
    Icon,
    Table,
    Step,
    Menu,
    Segment,
    Loader,
    Button,
    Message,
    Transition,
    Modal,
    Checkbox, Dropdown, Input, List
} from 'semantic-ui-react';
import "../Design/ProgramareExamene.css";
import {getUsername} from "../Services/GetUsername";
import {
    getAnUnivCurent,
    getExameneProgramateProfesor,
    getSesiuneExameneSpecializare,
    getMateriiProfesor,
    getSesiuniActive,
    existaExamenAceeasiZiOraProfesor,
    programareExamenMerge,
    getIdProfesor,
    getSaliByID_Facultate,
    existaExamenAceeasiZiSpecializare,
    salaEsteOcupata,
    orarSali, setariUniversitare
} from "../Services/Functions";
import Sigla from "../Components/Sigla";
import moment from "moment";
import {v4 as uuidv4} from 'uuid';
import TableRowComponent from "../Components/TableRowComponent";
import CalendarSaliComponent from "../Components/CalendarSaliComponent";
import "antd/dist/antd.css"; //todo overwrite css
import {TimePicker} from 'antd';
import DatePicker from "react-datepicker";


const timeoutLength = 3500

const durataExamenMinute = () => {
    let outputDurata = []
    for (let i = 15; i <= 180; i = i + 15) {
        outputDurata.push(i)
    }
    return outputDurata
}

const isWeekday = (date) => {
    const day = date.getDay();
    return day !== 0 && day !== 6;
};

class ProgramareExamene extends Component {

    state = {
        materiiProfesor: [],
        facultatiProfesor: [], //faculties for filter
        specializariProfesor: [], //specializations for filter
        aniStudiuProfesor: [], //year of study for filter
        sesiuniActiveSpecializare: [],
        semestruDinAn: null,
        semestruDinAnSelectat: null,
        anUniversitarCurent: null,
        anStudiuSelectat: null,  //dropdown anStudiu
        facultateaSelectata: null,  //dropdown facultate
        specializareSelectata: null, //dropdown specializare
        listaTipDurataExamen: [{key: 0, value: 1, label: "1h"}, {key: 1, value: 2, label: "2h"}],
        listaTipExamen: [{key: 0, value: 1, label: "Fizic"}, {key: 1, value: 2, label: "Online"}], //dropdown examination type
        listaTipExaminare: [{key: 0, value: 1, text: "Scris"}, {key: 1, value: 2, text: "Oral"}], //dropdown exam type
        activeRow: -1, //used to disable rows when 'Edit' button is clicked
        disableRow: false, //change its value to true when 'Edit' bttn is clicked, otherwise by defauld all the rows are disabled
        existaSesiuniDeProgramat: true, //takes the value TRUE if the current time is in the period when the exams can be scheduled
        primitDateResponse: false,
        primitDateCalendar: false,
        sesiuneExamenSelectata: null, //in case there are several active sessions
        listaSesiuniActive: [], //list of sessions - if applicable
        exameneProgramateProfesor: [],//scheduled exams from the database
        exameneProfesorAsistent: [],
        afisareMateriiTitular: true,
        checked: false, //checkbox
        obiectDeInlocuit: null,//to be able to return to the previous value in DatePicker if Changes are not saved
        visible: false, // for message of successful scheduling of an exam
        listaSaliFacultate: [],
        listaSemestre: [{key: 0, value: 1, text: "Semestrul I"}, {key: 1, value: 2, text: "Semestrul II"}],
        materiiProgramateSecretariat: [],
        modalSali: false,
        exameneOrar: [], //list of all the exams from the database
        examenDeProgramatInOrar: [], //the selected/edited exam
        salaSelectata: null, //dropdown din orar
        modalConfirmareTimeSlot: false,
        calendarSelectedTimeSlotStart: null,
        calendarSelectedTimeSlotEnd: null,
        valueTimePickerCalendar: null,
        durataTimePickerCalendar: '',
        examenOnline: false,
        setariProgramare:[], //lista de reguli setari programare
        setareProgramarePeFacultate:null
    }

    getInfoMaterii = async (idAnUniv, idSemestru, idFacultate) => {
        let exameneProfesor = []
        let exameneProfesorAsistent = []

        getSesiuneExameneSpecializare(this.state.username, idAnUniv)
            .then(sesiuneSpecializare => {
                let listaSesiuniSpecializare = []

                if (idFacultate !== null) {
                    sesiuneSpecializare.forEach(data => {
                        if (data.ID_Facultate === idFacultate && data.ID_SesiuneExamene === this.state.sesiuneExamenSelectata.value && (data.ID_TipProfesorAsociere === 1 || data.ID_TipProfesorAsociere === 3)) {
                            let sesiuniSpecializare = {
                                ID_SesiuneExamene: data.ID_SesiuneExamene,
                                value: data.ID_SesiuneExameneSpecializare, //pentru functie
                                ID_Facultate: data.ID_Facultate,
                                ID_Specializare: data.ID_Specializare,
                                ID_AnStud: data.ID_AnStud,
                                ID_TipFormaInv: data.ID_TipFormaInv,
                                Denumire: data.Denumire,
                                ID_TipCiclu: data.ID_TipCiclu
                            }
                            this.formatareDateFiltre(listaSesiuniSpecializare, data.ID_SesiuneExameneSpecializare, sesiuniSpecializare)
                        }
                    })
                }

                //get the ID_ExamSessionSpecialization which is unic for every ID_ExamSession
                getExameneProgramateProfesor(idAnUniv, this.state.username)
                    .then(exameneProgramate => {
                        //idFacultate is null when the semester changes to display only the subjects
                        if (idFacultate !== null) {
                            exameneProgramate.forEach((element, index) => {
                                if (element.ID_Facultate === idFacultate && element.ID_SesiuneExamene === this.state.sesiuneExamenSelectata.value && element.NumarSemestruDinAn === idSemestru) {
                                    let examene = {
                                        key: uuidv4(),
                                        indexRow: index,
                                        ID_Materie: element.ID_Materie,
                                        ID_Facultate: element.ID_Facultate,
                                        ID_Specializare: element.ID_Specializare,
                                        ID_Grupe: element.ID_Grupa,
                                        DataProgramareExamen: element.Data,
                                        ID_AnStud: element.ID_AnStud,
                                        Sala: element.Sala,
                                        ID_TipExaminare: element.ID_TipExaminare,
                                        ID_DetaliuPlanSemestru: element.ID_DetaliuPlanSemestru,
                                        ID_SesiuneExamene: element.ID_SesiuneExamene,
                                        ID_SesiuneExameneSpecializare: element.ID_SesiuneExameneSpecializare,
                                        ID_TipProfesorAsociere: element.ID_TipProfesorAsociere,
                                        DenumireSpecializare: element.DenumireSpecializare,
                                        Denumire: element.Denumire,
                                        DenumireScurtaFacultate: element.DenumireScurtaFacultate,
                                        ID_TipFormaInv: element.ID_TipFormaInv,
                                        DenumireAnStudiu: element.DenumireAnStudiu,
                                        DenumireGrupa: element.DenumireGrupa,
                                        NumarSemestruDinAn: element.NumarSemestruDinAn,
                                        DenumireTipProfesorAsociere: element.DenumireTipProfesorAsociere,
                                        DurataExamen: element.DurataExamen,
                                        ID_Sala: element.ID_Sala
                                    }
                                    if (examene.ID_TipProfesorAsociere === 2 || examene.ID_TipProfesorAsociere === 4) {
                                        exameneProfesorAsistent.push(examene)
                                    } else {
                                        exameneProfesor.push(examene)
                                    }
                                }
                            })
                            this.setState({
                                exameneProgramateProfesor: exameneProfesor,
                                exameneProfesorAsistent: exameneProfesorAsistent
                            })
                        }
                        getMateriiProfesor(idAnUniv, this.state.username, idSemestru)
                            .then(materii => {
                                this.setState({
                                    primitDateResponse: true
                                })

                                let listaMateriiProfesor = [];
                                let listaFacultati = [];
                                let listaSpecializari = [];
                                let listaAniStudiu = [];


                                materii.forEach((it, index) => {
                                    if (it.ID_Facultate === idFacultate || idFacultate === null) {
                                        let materiiProfesor = {
                                            key: uuidv4(),
                                            indexRow: index,
                                            isExpanded: false,
                                            ID_Materie: it.ID_Materie,
                                            Denumire: it.Denumire,
                                            ID_TipCiclu: it.ID_TipCiclu,
                                            DenumireCicluInv: it.DenumireCicluInv,
                                            ID_Facultate: it.ID_Facultate,
                                            DenumireFacultate: it.DenumireFacultate,
                                            ID_TipFormaInv: it.ID_TipFormaInv,
                                            DenumireFormaInv: it.DenumireFormaInv,
                                            ID_Specializare: it.ID_Specializare,
                                            DenumireSpecializare: it.DenumireSpecializare,
                                            ID_AnUniv: it.ID_AnUniv,
                                            ID_Profesor: it.ID_Profesor,
                                            ID_ProfesorTitularCurs: it.ID_ProfesorTitularCurs,
                                            DenumireAnStudiu: it.DenumireAnStudiu,
                                            NumarSemestruDinAn: it.NumarSemestruDinAn,
                                            DenumireScurtaFacultate: it.DenumireScurtaFacultate,
                                            Grupe: [{
                                                key: uuidv4(),
                                                Nume: it.Nume,
                                                ID_Grupe: it.ID_Grupe,
                                                DataProgramareExamen: null,
                                                Sala: null,
                                                NrStudentiGrupa: it.NrStudentiGrupa,
                                                DurataExamen: null
                                            }],
                                            NrStudentiGrupa: null,
                                            DataProgramareExamen: null, //pentru specializarile cu mai multe grupe
                                            ID_TipExaminare: null,
                                            Sala: [],
                                            ID_AnStud: it.ID_AnStud,
                                            ID_DetaliuPlanSemestru: it.ID_DetaliuPlanSemestru,
                                            ID_SesiuneExameneSpecializare: null,
                                            NrStudentiRestantieri: it.NrStudentiRestantieri,
                                            DurataExamen: null
                                        };

                                        let specializari = {
                                            key: index,
                                            value: it.ID_Specializare,
                                            text: it.DenumireScurtaSpecializare + ", " + it.DenumireFormaInv
                                        }

                                        let aniStudiu = {
                                            key: index,
                                            value: it.DenumireAnStudiu,
                                            text: it.DenumireAnStudiu
                                        }

                                        this.formatareDateFiltre(listaSpecializari, it.ID_Specializare, specializari)
                                        this.formatareDateFiltre(listaAniStudiu, it.DenumireAnStudiu, aniStudiu)

                                        // check if there is already a scheduled exam for this group
                                        const filterExamene = exameneProfesor.find(examen => examen.ID_Materie === it.ID_Materie && examen.ID_Specializare === it.ID_Specializare && examen.ID_Grupe === it.ID_Grupe && examen.ID_SesiuneExamene === this.state.sesiuneExamenSelectata.value && examen.ID_AnStud === it.ID_AnStud)

                                      //todo de vazut de ce nu il ia  if (it.ID_Specializare===10421) {console.log(filterExamene)}


                                        //add TipExaminare for the object materiiProfesor
                                        if (filterExamene !== undefined) {
                                            let indexExamen = exameneProfesor.indexOf(filterExamene)
                                            exameneProfesor.splice(indexExamen, 1)
                                            materiiProfesor.ID_TipExaminare = filterExamene.ID_TipExaminare
                                            //se sterg obiectele din lista de exameneProfesor pentru ca cele care raman
                                            // sunt programate de secretariat (ex: restantieri)
                                        }

                                        //add ID_SesiuneExameneSpecializare
                                        const filterSesiuniSpecializare = listaSesiuniSpecializare.find(sesiuneSpecializare => sesiuneSpecializare.ID_Specializare === it.ID_Specializare && sesiuneSpecializare.ID_SesiuneExamene === this.state.sesiuneExamenSelectata.value && sesiuneSpecializare.ID_AnStud === it.ID_AnStud && sesiuneSpecializare.ID_TipFormaInv === it.ID_TipFormaInv && sesiuneSpecializare.ID_TipCiclu === it.ID_TipCiclu)
                                        if (filterSesiuniSpecializare !== undefined) {
                                            materiiProfesor.ID_SesiuneExameneSpecializare = filterSesiuniSpecializare.value
                                        }


                                        //create the obj for every group of students
                                        if (listaMateriiProfesor.length > 0) {
                                            let specializare = listaMateriiProfesor.filter((item) => item.ID_Materie === it.ID_Materie && item.ID_AnUniv === it.ID_AnUniv && item.ID_Facultate === it.ID_Facultate && item.ID_TipFormaInv === it.ID_TipFormaInv && item.ID_Specializare === it.ID_Specializare && item.ID_TipCiclu === it.ID_TipCiclu && item.ID_AnStud === it.ID_AnStud && item.NumarSemestruDinAn === it.NumarSemestruDinAn)
                                            //check if the current group is already added in Grupe obj
                                            if (specializare.length >= 1) {
                                                let grupa = {
                                                    key: uuidv4(),
                                                    Nume: it.Nume,
                                                    ID_Grupe: it.ID_Grupe,
                                                    DataProgramareExamen: null,
                                                    Sala: null,
                                                    NrStudentiGrupa: it.NrStudentiGrupa,
                                                    DurataExamen: null
                                                }

                                                //check if the current group has an exam scheduled
                                                if (filterExamene !== undefined) {
                                                    this.formatGrupa(grupa, filterExamene.DataProgramareExamen, filterExamene.ID_Sala, filterExamene.Sala, filterExamene.DurataExamen)
                                                }

                                                specializare[0].Grupe.push(grupa);

                                            } else {
                                                //if the field of study doesn't have more than a group
                                                if (filterExamene !== undefined) {
                                                    this.formatGrupa(materiiProfesor.Grupe[0], filterExamene.DataProgramareExamen, filterExamene.ID_Sala, filterExamene.Sala, filterExamene.DurataExamen)
                                                }
                                                listaMateriiProfesor.push(materiiProfesor)
                                            }
                                        } else {
                                            //if the obj is empty
                                            if (filterExamene !== undefined) {
                                                this.formatGrupa(materiiProfesor.Grupe[0], filterExamene.DataProgramareExamen, filterExamene.ID_Sala, filterExamene.Sala, filterExamene.DurataExamen)
                                            }
                                            listaMateriiProfesor.push(materiiProfesor)
                                        }

                                    }
                                });
                                console.log(listaMateriiProfesor)
                                this.formatDataSalaSpecializare(listaMateriiProfesor)

                                //adauga materii programate de secretariat
                                let materiiConcatenate
                                if (exameneProfesor.length >= 1) {
                                    exameneProfesor.forEach(examen =>
                                        examen['examenProgramatDeSecretariat'] = true
                                    )
                                    materiiConcatenate = listaMateriiProfesor.concat(exameneProfesor)
                                } else {
                                    materiiConcatenate = [...listaMateriiProfesor]
                                }

                                this.setState({
                                    materiiProfesor: materiiConcatenate.sort((data1, data2) =>
                                        new Date(data1.DataProgramareExamen).getTime() < new Date(data2.DataProgramareExamen).getTime() ? -1 : 1),
                                    facultatiProfesor: listaFacultati,
                                    specializariProfesor: listaSpecializari.sort((specializare1, specializare2) =>
                                        specializare1.text < specializare2.text ? -1 : 1),
                                    aniStudiuProfesor: listaAniStudiu.sort((anStudiu1, anStudiu2) =>
                                        anStudiu1.text < anStudiu2.text ? -1 : 1)
                                })

                            })
                    })
            })
    };


    componentDidMount() {
        const username = getUsername();
        getIdProfesor(username)
            .then(idProfesor => {
                this.setState({username: idProfesor.ID_Profesor});
                let semestruCurent = null
                getAnUnivCurent()
                    .then((anUniv) => {
                        moment() <= anUniv.DataSfirsitSem1 ?
                            semestruCurent = 1 : semestruCurent = 2
                        this.setState({
                            anUniversitarCurent: anUniv.ID_AnUniv,
                            semestruDinAn: semestruCurent,
                            semestruDinAnSelectat: semestruCurent
                        })

                        getSesiuniActive(anUniv.ID_AnUniv, idProfesor.ID_Profesor)
                            .then(response => {
                                    let sesiuniActive = []
                                    if (response !== undefined) {

                                        response.forEach(sesiune => {
                                            let idSesiuneActiva = {
                                                key: sesiune.ID_SesiuneExamene,
                                                value: sesiune.ID_SesiuneExamene,
                                                text: sesiune.DenumireSesiuneExamene,
                                                image: <Sigla
                                                    icon={sesiune.ID_Facultate}
                                                    size={"large"}
                                                    style={{marginRight: "5px"}}
                                                />,
                                                semaipotprogramaexamene: sesiune.ProfesoriiPotProgramaExamene,
                                                datainceputpefacultate: sesiune.DataInceput,
                                                datasfarsitpefacultate: sesiune.DataSfarsit,
                                                id_facultate: sesiune.ID_Facultate
                                            }
                                            sesiuniActive.push(idSesiuneActiva)
                                        })


                                        this.setState({
                                            listaSesiuniActive: sesiuniActive,//lista de sesiuni in cazul in care exista mai multe in aceeasi perioada
                                            sesiuneExamenSelectata: sesiuniActive[0]  //todo de modificat cu 0
                                        })
                                        this.listaSaliFacultate(sesiuniActive[0].id_facultate)
                                        this.getInfoMaterii(anUniv.ID_AnUniv, semestruCurent, sesiuniActive[0].id_facultate) //todo de modificat cu 0
                                        setariUniversitare().then(response=> {
                                            response[0].valoare.forEach(regula=>{
                                                if (regula.ID_Facultate.includes(sesiuniActive[0].id_facultate)===true){
                                                    this.setState({setareProgramarePeFacultate:regula.NrZile})
                                                }
                                            })
                                            this.setState({setariProgramare: response[0].valoare})
                                            }
                                        )
                                    } else {
                                        this.setState({existaSesiuniDeProgramat: false})
                                    }
                                }
                            )
                    })
            })
    }

    listaSaliFacultate = (ID_Facultate) => {
        getSaliByID_Facultate(ID_Facultate)
            .then(response => {
                let listaSali = []
                listaSali.push({key: -1, value: -1, text: 'Online', numecompletsala: 'Online'})
                response.forEach(sala => {
                    let sali = {
                        key: sala.ID_Sala,
                        value: sala.ID_Sala,
                        nrlocuri: sala.NrLocuri,
                        numecompletsala: sala.NumeCompletSala,
                        text:
                            <span>
                                {sala.NumeCompletSala}
                                <Icon style={{marginRight: "0"}} name={'male'} color={'grey'}/>
                                <strong>{sala.NrLocuri}</strong>
                            </span>
                    }
                    listaSali.push(sali)
                })
                this.setState({listaSaliFacultate: listaSali})
            })
    }

    //dropdown-uri meniu
    selectieSemestru = (e, semestruSelectat) => {
        if (this.state.semestruDinAn !== semestruSelectat.value) {
            this.getInfoMaterii(this.state.anUniversitarCurent, semestruSelectat.value, null)
        } else {
            this.listaSaliFacultate(this.state.sesiuneExamenSelectata.id_facultate)
            this.getInfoMaterii(this.state.anUniversitarCurent, semestruSelectat.value, this.state.sesiuneExamenSelectata.id_facultate)
        }

        this.setState({
            primitDateResponse: false,
            // facultateaSelectata: null,
            anStudiuSelectat: null,
            specializareSelectata: null,
            semestruDinAnSelectat: semestruSelectat.value,
            activeRow: -1,
            disableRow: false,
            afisareMateriiTitular: true,
            checked: false
        })
        this.deselectareGrupe(false)
    }
    selectieSesiuneExamene = (e, sesiuneSelectata) => {
        const sesiuneActiva = this.state.listaSesiuniActive.find(sesiune => sesiune.value === sesiuneSelectata.value)
        this.getSetariProgramare(sesiuneActiva.id_facultate)
        this.setState({
            sesiuneExamenSelectata: sesiuneActiva,
            primitDateResponse: false,
            activeRow: -1,
            disableRow: false,
            // facultateaSelectata: null,
            anStudiuSelectat: null,
            specializareSelectata: null,
            afisareMateriiTitular: true,
            checked: false
        })
        this.listaSaliFacultate(sesiuneActiva.id_facultate)
        this.getInfoMaterii(this.state.anUniversitarCurent, this.state.semestruDinAnSelectat, sesiuneActiva.id_facultate)
    }
    selectieAnStudiu = (e, anSelectat) => {
        anSelectat.value ? this.setState({anStudiuSelectat: anSelectat.value}) : this.setState({anStudiuSelectat: null})
        this.setState({
            activeRow: -1,
            disableRow: false,
            checked: false,
        })
        this.deselectareGrupe(false)
    }
    selectieSpecializare = (e, specializareSelectata) => {
        specializareSelectata.value ? this.setState({specializareSelectata: specializareSelectata.value}) : this.setState({specializareSelectata: null})
        this.setState({
            activeRow: -1,
            disableRow: false,
            checked: false,
        })
        this.deselectareGrupe(false)
    }

    getSetariProgramare=(ID_Facultate)=>{
        const setariProgramareCopy=[...this.state.setariProgramare]
        setariProgramareCopy.forEach(setare=>{
            if (setare.ID_Facultate.includes(ID_Facultate)===true){
                this.setState({setareProgramarePeFacultate:setare.NrZile})
                console.log(setare.NrZile)
            }
        })
    }

    //handle valori dropdown
    handleChangeDate = (e, objSpecializare, objGrupa) => {
        this.actualizareDataSalaTabel(objSpecializare, e, null, null, objGrupa)
    }
    handleTipExaminare = (e, tipExaminare, specializare) => {
        this.actualizareDataSalaTabel(specializare, null, null, tipExaminare.value, null)
    }
    handleChangeSala = (e, sala, specializare, grupa) => {
        const salaSelectata = sala.options.filter(optiuni => optiuni.value === sala.value)
        this.actualizareDataSalaTabel(specializare, null, salaSelectata[0], null, grupa)
    }

    updateListaSaliSpecializare = (objSpecializare) => {
        let listaSaliActualizata = []
        const verificaSalaComuna = objSpecializare.Grupe.every(data => data.Sala ? data.Sala.value === objSpecializare.Grupe[0].Sala.value:false)
       console.log(verificaSalaComuna)
        if (verificaSalaComuna) {
             listaSaliActualizata.push(objSpecializare.Grupe[0].Sala)
        } else {
            objSpecializare.Grupe.forEach(grupa => {
                if (grupa.Sala !== null) {
                    listaSaliActualizata.push(grupa.Sala)
                }
            })
        }
        return listaSaliActualizata
    }

    updateDataSpecializare=(objSpecializare)=>{
        let dataActualizata;
        const verificaDataComuna = objSpecializare.Grupe.every(data => new Date(data.DataProgramareExamen).getTime() === new Date(objSpecializare.Grupe[0].DataProgramareExamen).getTime())
        if (verificaDataComuna) {
            dataActualizata=new Date(objSpecializare.Grupe[0].DataProgramareExamen)
        } else {
            dataActualizata=null
        }
        return dataActualizata
    }
    updateDurataExamenSpecializare = (objSpecializare) => {
        let durataExamen = 0;
        let verificaDurataComuna = true;
        for (let i = 0; i < objSpecializare.Grupe.length; i++) {
            durataExamen += objSpecializare.Grupe[i].DurataExamen
            if (objSpecializare.Grupe[i].DurataExamen !== objSpecializare.Grupe[0].DurataExamen) {
                verificaDurataComuna = false
            }
        }
        if (verificaDurataComuna === true) {
            durataExamen = objSpecializare.Grupe[0].DurataExamen
        }

        return durataExamen
    }


    //verificari pentru suprapuneri
    verificaSala = async (data, sala, durata, specializare, anstudiu, materie, esteOnline) => {
        if (esteOnline) {
            return true;
        } else {
            return await salaEsteOcupata(this.state.anUniversitarCurent, data, sala, durata, specializare, anstudiu, materie)
                .then(response => {
                    console.log('FUNCTION verificaSala response ' + response)
                    if (response <= 0) {
                        alert("Aceasta sală nu mai are mai locuri disponibile. Modificați intervalul orar sau alegeți altă sală.")
                        return false;
                    } else {
                        let listaSali = [...this.state.listaSaliFacultate]
                        const filterSali = listaSali.find(sali => sali.value === sala)
                        let nrLocuriSala = filterSali.nrlocuri

                        console.log('nrTotalLocuriSala: ' + nrLocuriSala)
                        console.log('numar studenti grupa/specializare: ' + this.state.examenDeProgramatInOrar.NrStudentiGrupa)

                        if (response > 0 && response < this.state.examenDeProgramatInOrar.NrStudentiGrupa) {
                            //cazul in care sala a fost partial ocupata, else nu a fost ocupata deloc
                            return window.confirm("Aceasta sală are mai puține locuri disponibile decat numărul studenților din grupă. Doar " + response + " locuri disponibile.Sunteti sigur ca doriți să programați acest examen?");
                        } else {
                            return true;
                        }

                    }
                })
        }
    }
    verificaIntervalProfesor = async (data, ID_Specializare, ID_AnStud, ID_Materie) => {
        return await existaExamenAceeasiZiOraProfesor(this.state.anUniversitarCurent, this.state.sesiuneExamenSelectata.value, this.state.username, data, ID_Specializare, ID_AnStud, ID_Materie)
            .then(statusVerificaDataProfesor => {
                console.log('FUNCTION verificaIntervalProfesor response: ' + statusVerificaDataProfesor)
                if (statusVerificaDataProfesor !== 0) {
                    return window.confirm("Mai aveti un examen programat la aceeasi ora. Sunteti sigur ca doriti sa programati acest examen?");
                } else {
                    return true;
                }
            })
    }
    verificaSuprapunereExamenSpecializare = async (data) => {
        return await existaExamenAceeasiZiSpecializare(this.state.anUniversitarCurent, this.state.sesiuneExamenSelectata.value, this.state.examenDeProgramatInOrar.ID_Specializare, this.state.examenDeProgramatInOrar.ID_Materie, data, this.state.examenDeProgramatInOrar.ID_TipFormaInv, this.state.setareProgramarePeFacultate)
            .then(statusSpecializare => {
                console.log('FUNCTION verificaSuprapunereExamenSpecializare response: ' + statusSpecializare)
                if (statusSpecializare !== 0) {
                    return window.confirm(`Aceasta specializare mai are un examen programat in aceeasi zi sau in interval de ${this.state.setareProgramarePeFacultate} zile. Sunteti sigur ca doriti sa programati acest examen?`);
                } else {
                    return true;
                }
            })
    }


    //pentru a afisa in linia principala un array de sali/date sau o singura valoare
    formatDataSalaSpecializare = (listaMaterii) => {
        // for (let i = 0; i < listaMaterii.length; i++) {
        //     let outputSali = [];
        //     let nrStudentiSpecializare = 0;
        //     let durataExamenSpecializare = 0;
        //
        //    listaMaterii[i].DataProgramareExamen=this.updateDataSpecializare(listaMaterii[i])
        //     if (listaMaterii[i].DataProgramareExamen!==null){
        //         durataExamenSpecializare=listaMaterii[i].Grupe[0].DurataExamen
        //     }
        //     for (let j = 0; j < listaMaterii[i].Grupe.length; j++) {
        //         const lista = listaMaterii[i]
        //         if (lista.Grupe[j].Sala) {
        //             if (outputSali.find(obj => obj.value === lista.Grupe[j].Sala.value) === undefined) {
        //                 outputSali.push(lista.Grupe[j].Sala);
        //             }
        //         }
        //         if(lista.DataProgramareExamen===null){
        //             durataExamenSpecializare += lista.Grupe[j].DurataExamen
        //         }
        //         nrStudentiSpecializare += lista.Grupe[j].NrStudentiGrupa
        //     }
        //
        //     listaMaterii[i].Sala = [...outputSali]
        //
        //     listaMaterii[i].NrStudentiGrupa = nrStudentiSpecializare
        //     listaMaterii[i].DurataExamen = durataExamenSpecializare
        //
        // }
        // return listaMaterii;
        for (let i = 0; i < listaMaterii.length; i++) {
            let outputSali = [];
            let outputDataProgramareExamen = [];
            let nrStudentiSpecializare = 0;
            let durataExamenSpecializare = 0;
            for (let j = 0; j < listaMaterii[i].Grupe.length; j++) {
                const lista = listaMaterii[i]
                if (lista.Grupe[j].Sala) {
                    if (outputSali.find(obj => obj.value === lista.Grupe[j].Sala.value) === undefined) {
                        outputSali.push(lista.Grupe[j].Sala);
                    }
                }
                if (lista.Grupe[j].DataProgramareExamen) {
                    if (!outputDataProgramareExamen.find(data => new Date(data).getTime() === (new Date(lista.Grupe[j].DataProgramareExamen)).getTime())) {
                        outputDataProgramareExamen.push(new Date(lista.Grupe[j].DataProgramareExamen))
                    }

                }
                //durata ramane aceeasi daca datele de inceput sunt aceleasi, altfel durata afisata in prima linie e suma duratei grupelor
                if (outputDataProgramareExamen.length <= 1) {
                    durataExamenSpecializare = lista.Grupe[j].DurataExamen
                } else {
                    durataExamenSpecializare += lista.Grupe[j].DurataExamen
                }
                nrStudentiSpecializare += lista.Grupe[j].NrStudentiGrupa
            }

            listaMaterii[i].Sala = [...outputSali]

            if (outputDataProgramareExamen.length === 1) {
                listaMaterii[i].DataProgramareExamen = new Date(outputDataProgramareExamen)
            }
            listaMaterii[i].NrStudentiGrupa = nrStudentiSpecializare
            listaMaterii[i].DurataExamen = durataExamenSpecializare
        }
        return listaMaterii;
    }

    actualizareDataSalaTabel = (objSpecializare, data, sala, tipExaminare, objGrupa) => {
        //data=null =>se va actualiza sala;
        // sala=null=>se va actualiza data

        let obiectAnterior = JSON.parse(JSON.stringify(objSpecializare))
        // object saved when the Edit Mode is closed without saving the changes to go back to the previous value
        this.state.obiectDeInlocuit === null &&
        this.setState({obiectDeInlocuit: obiectAnterior})

        let materiiProfesorCopy = [...this.state.materiiProfesor]
        const obiectRezultat = materiiProfesorCopy.filter(data => data === objSpecializare)
        if (obiectRezultat !== undefined) {
            console.log('actualizareDataSalaTabel', obiectRezultat[0])
            if (tipExaminare !== null) {
                obiectRezultat[0].ID_TipExaminare = tipExaminare
            }
            //o grupa
            else if (objGrupa !== null) {
                if (data !== null && obiectRezultat[0].DataProgramareExamen !== null) {
                    obiectRezultat[0].DataProgramareExamen = null
                }
                obiectRezultat[0].Grupe.forEach(grupe => {
                    if (grupe === objGrupa) {
                        console.log('grupe')
                        data ?
                            grupe.DataProgramareExamen = new Date(data) :
                            grupe.Sala = {
                                key: sala.key,
                                value: sala.value,
                                text: sala.numecompletsala
                            }
                    }
                })
                if (sala !== null) {
                    console.log('if sala!==null')
                    obiectRezultat[0].Sala = this.updateListaSaliSpecializare(obiectRezultat[0])
                }
            }

            //toate grupele
            else {
                data ? obiectRezultat[0].DataProgramareExamen = new Date(data) :
                    obiectRezultat[0].Sala = [{
                        key: sala.key,
                        text: sala.numecompletsala,
                        value: sala.value
                    }]
                obiectRezultat[0].Grupe.forEach(grupe => {
                    data ?
                        grupe.DataProgramareExamen = new Date(data) :
                        grupe.Sala = {
                            key: sala.key,
                            text: sala.numecompletsala,
                            value: sala.value
                        }
                })
            }
        }
        this.setState({materiiProfesor: materiiProfesorCopy})
        console.log('actualizareDataSalaTabel', materiiProfesorCopy)
    }

    formatareDateFiltre = (lista, ID, obiect) => {
        if (lista && lista.length >= 1) {
            //filter ia valoarea undefined cand nu gaseste DenumireanStudiu
            let filter = lista.find((item) => item.value === ID)
            if (filter === undefined) {
                lista.push(obiect)
            }
        } else {
            lista.push(obiect)
        }
        return lista
    }

    deselectareGrupe = (value) => {
        const materiiProfesorCopy = [...this.state.materiiProfesor]
        for (let i = 0; i < materiiProfesorCopy.length; i++) {
            materiiProfesorCopy[i].isExpanded = value
        }
        this.setState({materiiProfesor: materiiProfesorCopy})
    }

    handleExpandedRows = (e, titleProps) => {
        const {index} = titleProps;
        const materiiProfesorCopy = [...this.state.materiiProfesor]

        for (let i = 0; i < materiiProfesorCopy.length; i++) {
            if (materiiProfesorCopy[i].indexRow === index) {
                materiiProfesorCopy[i].isExpanded = !materiiProfesorCopy[i].isExpanded;
            }

        }
        this.setState({materiiProfesor: materiiProfesorCopy})
    }

    handleRowDisabled = (e, titleProps) => {
        const {index} = titleProps;
        this.setState({activeRow: index, disableRow: true})
    }

    revenireObiectAnterior = () => {
        let materiiProfesorCopy = [...this.state.materiiProfesor]
        let obj = {...this.state.obiectDeInlocuit}

        if (this.state.obiectDeInlocuit !== null) {
            materiiProfesorCopy.forEach(element => {
                if (element.ID_Facultate === obj.ID_Facultate && element.ID_Materie === obj.ID_Materie && element.ID_Specializare === obj.ID_Specializare) {
                    this.formatSpecializare(element, obj, false)
                }
            })
        }
        this.setState({materiiProfesor: materiiProfesorCopy, obiectDeInlocuit: null})
    }

    cancelDisabledRows = () => {
        //cand se apasa pe butonul 'renunta la modifcari (tip examen/data/sala)'
        this.revenireObiectAnterior()
        this.setState({activeRow: -1, disableRow: false})
    }

    actualizareDateExamenProgramat = (examenProgramatInOrarCopy) => {
        let materiiProfesorCopy = [...this.state.materiiProfesor]

        console.log('cum ar trb sa arate', examenProgramatInOrarCopy)
        const obiectDeActualizat = materiiProfesorCopy.find(element => element.ID_Facultate === examenProgramatInOrarCopy.ID_Facultate && element.ID_Materie === examenProgramatInOrarCopy.ID_Materie && element.ID_Specializare === examenProgramatInOrarCopy.ID_Specializare && element.ID_AnStud === examenProgramatInOrarCopy.ID_AnStud)
        if (obiectDeActualizat !== undefined) {
            // pentru specializare
            if (examenProgramatInOrarCopy.ID_Grupa === null) {
                this.formatSpecializare(obiectDeActualizat, examenProgramatInOrarCopy, true)
            }
            //programare pentru o grupa
            else {
                for (let i = 0; i < obiectDeActualizat.Grupe.length; i++) {
                        if (obiectDeActualizat.Grupe[i].ID_Grupe === examenProgramatInOrarCopy.ID_Grupa) {
                            this.formatGrupa(obiectDeActualizat.Grupe[i], examenProgramatInOrarCopy.DataProgramareExamen, examenProgramatInOrarCopy.ID_Sala, examenProgramatInOrarCopy.Sala, examenProgramatInOrarCopy.DurataExamen)
                        }
                        else if (this.state.obiectDeInlocuit!==null) {
                            if (obiectDeActualizat.Grupe[i].ID_Grupe === this.state.obiectDeInlocuit.Grupe[i].ID_Grupe) {
                                console.log('intra obiectdDeInlocuit', this.state.obiectDeInlocuit.Grupe[i])
                                this.formatGrupa(obiectDeActualizat.Grupe[i], this.state.obiectDeInlocuit.Grupe[i].DataProgramareExamen, this.state.obiectDeInlocuit.Grupe[i].Sala ? this.state.obiectDeInlocuit.Grupe[i].Sala.value:null, this.state.obiectDeInlocuit.Grupe[i].Sala? this.state.obiectDeInlocuit.Grupe[i].Sala.text:null, this.state.obiectDeInlocuit.Grupe[i].DurataExamen)
                            }
                        }
                }
                obiectDeActualizat.Sala=this.updateListaSaliSpecializare(obiectDeActualizat)
                obiectDeActualizat.DataProgramareExamen=this.updateDataSpecializare(obiectDeActualizat)
                obiectDeActualizat.DurataExamen=this.updateDurataExamenSpecializare(obiectDeActualizat)
                obiectDeActualizat.isExpanded=true
            }
        }
        console.log('rezultat final actualizareDateExamenProgramat: ' , materiiProfesorCopy)
        this.setState({materiiProfesor: materiiProfesorCopy,
            activeRow: -1,
            disableRow: false,
            obiectDeInlocuit:null})
    }

    formatSpecializare = (obiectDeActualizat, obiectAnterior, esteFormatComunGrupe) => {
        console.log('formatS', obiectAnterior, )
        if (obiectAnterior.DataProgramareExamen === null) {
            obiectDeActualizat.DataProgramareExamen = null
        } else {
            obiectDeActualizat.DataProgramareExamen = new Date(obiectAnterior.DataProgramareExamen)
        }
        esteFormatComunGrupe ?
            obiectDeActualizat.Sala = [{
                key: obiectAnterior.ID_Sala,
                value: obiectAnterior.ID_Sala,
                text: obiectAnterior.Sala
            }] :
            obiectDeActualizat.Sala = [...obiectAnterior.Sala]
        obiectDeActualizat.ID_TipExaminare = obiectAnterior.ID_TipExaminare
        obiectDeActualizat.DurataExamen = obiectAnterior.DurataExamen

        console.log('formatS update', obiectDeActualizat, obiectAnterior)
        for (let i = 0; i < obiectDeActualizat.Grupe.length; i++) {
            esteFormatComunGrupe ?
                this.formatGrupa(obiectDeActualizat.Grupe[i], obiectAnterior.DataProgramareExamen, obiectAnterior.ID_Sala, obiectAnterior.Sala, obiectAnterior.DurataExamen) :
                this.formatGrupa(obiectDeActualizat.Grupe[i], obiectAnterior.Grupe[i].DataProgramareExamen, obiectAnterior.Grupe[i].Sala? obiectAnterior.Grupe[i].Sala.value:null, obiectAnterior.Grupe[i].Sala ? obiectAnterior.Grupe[i].Sala.text:null, obiectAnterior.Grupe[i].DurataExamen)
        }
    }

    formatGrupa = (grupa, data, id_Sala, denumireSala, durataExamen) => {
        if (data!==null) {
            grupa.DataProgramareExamen = new Date(data)
        }
        else{
            grupa.DataProgramareExamen=null
        }
        if (id_Sala!==null) {
            grupa.Sala = {
                key: id_Sala,
                value: id_Sala,
                text: denumireSala
            }
        }
        else{
            grupa.Sala=null
        }
        grupa.DurataExamen = durataExamen
    }

    programeazaExamen = () => {
        let examenDeProgramatInOrarCopy = {...this.state.examenDeProgramatInOrar}; let data

        //verifica in care dintre cele doua modale suntem
        if (this.state.valueTimePickerCalendar === null) {
            //al doilea modal
            data = this.state.calendarSelectedTimeSlotStart._d
            examenDeProgramatInOrarCopy.DurataExamen = moment(this.state.calendarSelectedTimeSlotEnd).diff(moment(this.state.calendarSelectedTimeSlotStart), 'minutes')
        } else {
            //primul modal
            data = this.state.valueTimePickerCalendar._d
            examenDeProgramatInOrarCopy.DurataExamen = this.state.durataTimePickerCalendar
        }
        examenDeProgramatInOrarCopy.DataProgramareExamen = new Date(new Date(examenDeProgramatInOrarCopy.DataProgramareExamen).setHours(data.getHours(), data.getMinutes()));

        let esteOnline = false;
        if (examenDeProgramatInOrarCopy.ID_Sala === -1) {
            esteOnline = true;
            examenDeProgramatInOrarCopy.Sala = 'Online'
        } else {
            examenDeProgramatInOrarCopy.Sala = this.state.listaSaliFacultate.find(sala => sala.value === examenDeProgramatInOrarCopy.ID_Sala).numecompletsala
        }

        console.log('examenDeProgramatInOrarCopy2', examenDeProgramatInOrarCopy)
        this.setState({examenDeProgramatInOrar: examenDeProgramatInOrarCopy})
        let dateProgramareExamen = {
            ID_DetaliuPlanSemestru: examenDeProgramatInOrarCopy.ID_DetaliuPlanSemestru,
            ID_SesiuneExameneSpecializare: examenDeProgramatInOrarCopy.ID_SesiuneExameneSpecializare,
            ID_Grupa: examenDeProgramatInOrarCopy.ID_Grupa === null ? examenDeProgramatInOrarCopy.Grupe[0].ID_Grupe : examenDeProgramatInOrarCopy.ID_Grupa,
            ID_Sala: examenDeProgramatInOrarCopy.ID_Sala,
            Sala: examenDeProgramatInOrarCopy.Sala,
            Data: moment(examenDeProgramatInOrarCopy.DataProgramareExamen).format('YYYY/MM/DD HH:mm:ss'),
            ID_TipExaminare: examenDeProgramatInOrarCopy.ID_TipExaminare,
            ID_Profesor: this.state.username,
            UserIDModificare: this.state.username,
            DurataExamen: examenDeProgramatInOrarCopy.DurataExamen
        }

        this.verificaSala(dateProgramareExamen.Data, dateProgramareExamen.ID_Sala, dateProgramareExamen.DurataExamen, examenDeProgramatInOrarCopy.ID_Specializare, examenDeProgramatInOrarCopy.ID_AnStud, examenDeProgramatInOrarCopy.ID_Materie, esteOnline)
            .then(resultVerificaSala => {
                    if (resultVerificaSala) {
                        this.verificaIntervalProfesor(dateProgramareExamen.Data, examenDeProgramatInOrarCopy.ID_Specializare, examenDeProgramatInOrarCopy.ID_AnStud, examenDeProgramatInOrarCopy.ID_Materie)
                            .then(resultVerificaDataProfesor => {
                                console.log(resultVerificaDataProfesor)
                                if (resultVerificaDataProfesor) {
                                    this.verificaSuprapunereExamenSpecializare(dateProgramareExamen.Data)
                                        .then(resultSuprapunereSpecializare => {
                                            if (resultSuprapunereSpecializare) {

                                                programareExamenMerge(dateProgramareExamen)
                                                    .then(status => {
                                                        console.log('status', status)
                                                            if (examenDeProgramatInOrarCopy.ID_Grupa === null) {
                                                                //pentru programarea unei specializari
                                                                for (let i = 1; i < examenDeProgramatInOrarCopy.Grupe.length; i++) {
                                                                    console.log('examenDeProgramatInOrarCopy.Grupe[i]', examenDeProgramatInOrarCopy.Grupe[i])
                                                                    dateProgramareExamen.ID_Grupa = examenDeProgramatInOrarCopy.Grupe[i].ID_Grupe
                                                                    programareExamenMerge(dateProgramareExamen)
                                                                }
                                                            }

                                                            this.state.visible === false &&
                                                            this.toggleVisibility()
                                                            this.setState({
                                                                modalConfirmareTimeSlot: false,
                                                                modalSali: false
                                                            })
                                                            //actualizarea modificarilor in interfata
                                                            this.actualizareDateExamenProgramat(examenDeProgramatInOrarCopy)
                                                    })
                                            }
                                        })
                                }
                            })
                    }
                }
            )
    }

    toggleVisibility = () => {
        this.setState((prevState) => ({visible: !prevState.visible}))
        this.timeout = setTimeout(() => {
            this.setState((prevState) => ({visible: !prevState.visible}))
        }, timeoutLength)
    }

    handleCloseOrarSali = () => {
        // this.revenireObiectAnterior()    //se revine la obiectul anterior inainte de a fi modificat - aceeasi functionalitate ca atunci cand se renunta la modificarile datelor unui examen
        this.setState({
            modalSali: false,
            valueTimePickerCalendar: null,
            durataTimePickerCalendar: '',
            examenDeProgramatInOrar: null
        })
    }
    afisareOrarSali = (obj, grupa) => {
        this.setState({modalSali: true})
        //o specializare cu singura grupa/toaata specializarea -> grupa null,  Data!=null, Grupe=array
        //o grupa din mai multe existente -> ID_Grupe exista, grupa!==null
        let obiectPrimit = {
            ID_DetaliuPlanSemestru: obj.ID_DetaliuPlanSemestru,
            ID_SesiuneExameneSpecializare: obj.ID_SesiuneExameneSpecializare,
            ID_Grupa: null,
            ID_Sala: null,
            Sala: null,
            ID_TipExaminare: obj.ID_TipExaminare,
            ID_Profesor: this.state.username,
            UserIDModificare: this.state.username,
            DurataExamen: obj.DurataExamen,
            NrStudentiGrupa: null,
            ID_AnStud: obj.ID_AnStud,
            ID_Materie: obj.ID_Materie,
            ID_Specializare: obj.ID_Specializare,
            Grupe: obj.Grupe,
            ID_Facultate: obj.ID_Facultate,
            ID_TipFormaInv:obj.ID_TipFormaInv
        }

        if (grupa === null) {
            obiectPrimit.ID_Grupa = null
            obiectPrimit.ID_Sala = obj.Sala[0].value
            obiectPrimit.DataProgramareExamen = obj.DataProgramareExamen
            obiectPrimit.NrStudentiGrupa = obj.NrStudentiGrupa
        } else {
            obiectPrimit.ID_Grupa = grupa.ID_Grupe
            obiectPrimit.ID_Sala = grupa.Sala.value
            obiectPrimit.DataProgramareExamen = grupa.DataProgramareExamen
            obiectPrimit.NrStudentiGrupa = grupa.NrStudentiGrupa
        }

        if (obiectPrimit.ID_Sala === -1) {
            console.log('E ONLINE')
        }

        console.log('afisareOrarSali obiectPrimit', obiectPrimit)

        this.setState({examenDeProgramatInOrar: obiectPrimit, modalSali: true}, () => {
            this.actualizareOrarSali(obiectPrimit.DataProgramareExamen, obiectPrimit.ID_Sala)
        })

    }

    actualizareOrarSali = (dataSelectata, salaSelectata) => {
        this.setState({primitDateCalendar: false})
        let examenDeProgramatInOrarCopy = {...this.state.examenDeProgramatInOrar}

        examenDeProgramatInOrarCopy.DataProgramareExamen = dataSelectata
        examenDeProgramatInOrarCopy.ID_Sala = salaSelectata


        let data = [];
        let esteSelectatOnline = false
        if (examenDeProgramatInOrarCopy.ID_Sala === -1) {
            esteSelectatOnline = true
        } else {
            orarSali(moment(dataSelectata).format('YYYY/MM/DD HH:mm:ss'), salaSelectata).then(response => {
                response.forEach(item => {
                        let exameneProgramate = {
                            key: item.ID_ProgramareExamen,
                            title: item.DenumireSpecializare + ", " + item.DenumireMaterie + ", " + item.DenumireGrupa + " - " + item.NrStudentiGrupa + " locuri ocupate",
                            start: new Date(new Date(item.Data).getFullYear(), new Date(item.Data).getMonth(), new Date(item.Data).getDate(), new Date(item.Data).getHours(), new Date(item.Data).getMinutes()),
                            end: new Date(new Date(item.DataSfarsit).getFullYear(), new Date(item.DataSfarsit).getMonth(), new Date(item.DataSfarsit).getDate(), new Date(item.DataSfarsit).getHours(), new Date(item.DataSfarsit).getMinutes()),
                            hexColor: item.SuntMaiMulteProgramari? 'FF951B' : '268cd8'
                        }
                        data.push(exameneProgramate)
                    }
                )
                console.log('rezultat actualizareOrarSali-orarSali', data)

                this.setState({exameneOrar: data, primitDateCalendar: true})
            })
        }
        console.log('actualizareOrarSali examenDeProgramatInOrarCopy',examenDeProgramatInOrarCopy)
        this.setState({
            salaSelectata: salaSelectata,
            valueTimePickerCalendar: null, //reinitialized with null when the date/classroom is changed
            durataTimePickerCalendar: '',
            examenDeProgramatInOrar: examenDeProgramatInOrarCopy,
            examenOnline: esteSelectatOnline
        })
    }

    handleCloseConfirmareTimeSlot = () => this.setState({
        modalConfirmareTimeSlot: false,
        calendarSelectedTimeSlotStart: null,
        calendarSelectedTimeSlotEnd: null,
    })

    handleCheckbox = () => {
        this.setState((prevState) => ({checked: !prevState.checked}))
        const stateCheckbox = !this.state.checked
        this.deselectareGrupe(stateCheckbox)
    }

    handleSelectTimeSlot = ({start, end}) => {
        const timeSlot = {
            start: moment(start, 'HH:mm '),
            end: moment(end, 'HH:mm')
        }

        this.setState({
            calendarSelectedTimeSlotStart: timeSlot.start,
            calendarSelectedTimeSlotEnd: timeSlot.end,
            modalConfirmareTimeSlot: true,
            valueTimePickerCalendar: null,
            durataTimePickerCalendar: ''
        })
    }

    onChangeTimePickerValue = (time, variableName) => {
        this.setState({[variableName]: moment(time._d, 'HH:mm'),})
    }

    eventStyleGetter = (event) => {
        const backgroundColor = '#' + event.hexColor;
        const style = {
            backgroundColor: backgroundColor,
            borderRadius: '10px',
            color: 'black',
            opacity: 0.8,
        };
        return {
            style: style
        };
    }

    render() {
        const {visible} = this.state
        const {modalSali} = this.state
        const {modalConfirmareTimeSlot} = this.state

        return (
            <div style={{marginTop: "20px", width: "100%"}}>
                <Transition visible={visible} animation='scale'>
                    <Message success compact
                             style={{
                                 width: "35%",
                                 position: "fixed",
                                 top: "20px",
                                 right: "20px",
                                 zIndex: "99"
                             }}>
                        <Icon name={'checkmark'} color={'green'}/> Modificarile au fost salvate cu
                        succes!
                    </Message>
                </Transition>

                <h1 style={{textAlign: "center"}}>Programarea Examenelor</h1>
                <h3 style={{
                    textAlign: "center",
                    marginTop: "0"
                }}> {this.state.semestruDinAnSelectat && "Semestrul " + this.state.semestruDinAnSelectat} {this.state.semestruDinAnSelectat === this.state.semestruDinAn && this.state.sesiuneExamenSelectata && " - " + this.state.sesiuneExamenSelectata.text}</h3>
                <Step.Group style={{
                    display: "flex",
                    justifyContent: "center", marginTop: "40px", marginBottom: "50px"
                }}>
                    <Step>
                        <Icon name={'edit'} color={'blue'}/>
                        <Step.Content>
                            <Step.Title>Editează</Step.Title>
                            <Step.Description>Selectează materia dorită apăsând butonul de
                                Edit</Step.Description>
                        </Step.Content>
                    </Step>

                    <Step>
                        <Icon name={'file text'} color={'yellow'}/>
                        <Step.Content>
                            <Step.Title>Completează</Step.Title>
                            <Step.Description>Completează toate câmpurile pentru a programa
                                examenul</Step.Description>
                        </Step.Content>
                    </Step>

                    <Step>
                        <Icon name={'check circle'} color={'green'}/>
                        <Step.Content>
                            <Step.Title>Salvează</Step.Title>
                        </Step.Content>
                    </Step>
                </Step.Group>

                <Menu style={{width: "100%", backgroundColor: "#549eb0"}}>
                    <Menu.Item>
                        {this.state.semestruDinAn === this.state.semestruDinAnSelectat &&
                        this.state.listaSesiuniActive.length > 1 &&
                        <Dropdown
                            selection
                            disabled={!this.state.primitDateResponse}
                            className="dropdownLeft"
                            placeholder="Sesiune examene"
                            options={this.state.listaSesiuniActive}
                            onChange={this.selectieSesiuneExamene}
                            value={this.state.sesiuneExamenSelectata.value}
                        />
                        }
                        <Dropdown
                            selection
                            disabled={!this.state.primitDateResponse}
                            clearable
                            className="dropdownLeft"
                            placeholder="Specializarea"
                            options={this.state.specializariProfesor}
                            onChange={this.selectieSpecializare}
                            value={this.state.specializareSelectata}
                        />
                        <Dropdown
                            selection
                            disabled={!this.state.primitDateResponse}
                            clearable
                            className="dropdownLeft2"
                            placeholder="Anul de studiu"
                            options={this.state.aniStudiuProfesor}
                            onChange={this.selectieAnStudiu}
                            value={this.state.anStudiuSelectat}
                        />
                    </Menu.Item>

                    <Menu.Menu position='right'>
                        <Menu.Item>
                            {this.state.semestruDinAnSelectat &&
                            <Dropdown
                                selection
                                disabled={!this.state.primitDateResponse}
                                className="dropdownRight"
                                placeholder="Semestrul"
                                options={this.state.listaSemestre}
                                onChange={this.selectieSemestru}
                                value={this.state.semestruDinAnSelectat}
                            />
                            }
                        </Menu.Item>
                    </Menu.Menu>

                </Menu>

                {this.state.modalSali &&
                <Modal
                    open={modalSali}
                    onClose={this.handleCloseOrarSali}
                    size='fullscreen'>
                    <Modal.Content>
                        <Modal.Description>
                            <Menu attached='top'
                                  className={'menuModal'}>
                                <div className={'contentMenuModal'}>
                                    <strong className={'marginRight'}>ORARUL SĂLII </strong>
                                    <Dropdown
                                        search selection
                                        className={'dropdownOrar'}
                                        placeholder={'Selectati alta sala'}
                                        value={this.state.salaSelectata}
                                        onChange={(e, sala) => this.actualizareOrarSali(this.state.examenDeProgramatInOrar.DataProgramareExamen, sala.value)}
                                        options={this.state.listaSaliFacultate}/>
                                </div>

                                <Menu.Menu position='right'>
                                    <div className={'contentMenuModal'}>
                                        <strong className={'marginRight'}> PENTRU</strong>
                                        {/*{this.state.examenDeProgramatInOrar && moment(this.state.examenDeProgramatInOrar.DataProgramareExamen).format("DD.MM.YYYY")}*/}
                                        <DatePicker
                                            dateFormat="dd/MM/yyyy"
                                            filterDate={isWeekday}
                                            selected={this.state.examenDeProgramatInOrar.DataProgramareExamen}
                                            onChange={(e) => this.actualizareOrarSali(e, this.state.examenDeProgramatInOrar.ID_Sala)}
                                            minDate={new Date(this.state.sesiuneExamenSelectata.datainceputpefacultate)}
                                            maxDate={new Date(this.state.sesiuneExamenSelectata.datasfarsitpefacultate)}
                                        />
                                    </div>

                                </Menu.Menu>
                            </Menu>
                            {this.state.examenOnline === false ?
                                this.state.primitDateCalendar === true ?
                                    <>
                                        {(this.state.exameneOrar && this.state.exameneOrar.length>0) &&
                                        <List horizontal>
                                            <List.Item>
                                                <Icon name={'square'} style={{color: '#268cd8'}}/>
                                                <List.Content verticalAlign='middle' style={{display: "inline"}}>Examen
                                                    programat</List.Content>
                                            </List.Item>
                                            <List.Item>
                                                <Icon name={'square'} style={{color: '#FF951B'}}/>
                                                <List.Content verticalAlign='middle' style={{display: "inline"}}>Examen
                                                    programat cu suprapunere</List.Content>
                                            </List.Item>
                                        </List>
                                        }
                                        <CalendarSaliComponent
                                            examenOnline={this.state.examenOnline}
                                            eventStyleGetter={this.eventStyleGetter.bind(this)}
                                            key={JSON.stringify(this.state.examenDeProgramatInOrar)}
                                            dataSelectata={this.state.examenDeProgramatInOrar.DataProgramareExamen}
                                            exameneOrar={this.state.exameneOrar}
                                            handleSelectTimeSlot={this.handleSelectTimeSlot.bind(this)}
                                        />
                                        <Message warning>
                                            <Icon name='help'/>
                                            Selectati intervalul examenului din calendar sau completati campurile de mai
                                            jos.
                                        </Message>
                                    </> :

                                    <Segment style={{height: "450px"}}>
                                        <Loader size={'big'} active><h3 style={{color: "black"}}>Se încarcă datele</h3>
                                        </Loader>
                                    </Segment> : null

                            }
                            <Segment
                                disabled={!this.state.primitDateCalendar && !this.state.examenOnline}
                                style={{paddingTop: "20px"}}>
                                <h4>Selectați un interval liber pentru a programa examenul:</h4>
                                <div style={{display: "inline-block"}}>
                                    <p style={{marginBottom: "0"}}> Oră început:</p>
                                    <TimePicker
                                        disabledHours={() => [0, 1, 2, 3, 4, 5, 6, 7, 21, 22, 23, 24]}
                                        value={this.state.valueTimePickerCalendar}
                                        format={'HH:mm'}
                                        onSelect={(time) => this.onChangeTimePickerValue(time, 'valueTimePickerCalendar')}
                                    />
                                </div>
                                <div style={{display: "inline-block", paddingLeft: "30px"}}>
                                    <p style={{marginBottom: "0", marginTop: "10px"}}>Durata examen (minute): </p>
                                    <Input list='durataExamen'
                                           type="number" min="0"
                                           placeholder='Durata examen'
                                           value={this.state.durataTimePickerCalendar}
                                           onChange={(e) => this.setState({durataTimePickerCalendar: parseInt(e.target.value)})}/>
                                    <datalist id='durataExamen'>
                                        {durataExamenMinute().map((durata, index) => {
                                                return (
                                                    <option key={index} value={durata}/>
                                                )
                                            }
                                        )}
                                    </datalist>
                                </div>
                            </Segment>
                        </Modal.Description>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button type={'button'} content='Renunță' onClick={this.handleCloseOrarSali}/>
                        <Button
                            disabled={!(this.state.durataTimePickerCalendar && this.state.valueTimePickerCalendar)}
                            icon='check'
                            color={'green'}
                            content='Programează examen'
                            onClick={this.programeazaExamen}
                        />
                    </Modal.Actions>
                    <Modal
                        open={modalConfirmareTimeSlot}
                        onClose={this.handleCloseConfirmareTimeSlot}
                        size='large'>
                        <Modal.Header>Confirmare date</Modal.Header>
                        <Modal.Content>
                            <p>Data: <b>{moment(this.state.examenDeProgramatInOrar.DataProgramareExamen).format('DD-MM-YYYY')}</b>
                            </p>
                            <p>Sala: <b>{this.state.listaSaliFacultate.find(sala => sala.value === this.state.examenDeProgramatInOrar.ID_Sala).numecompletsala}</b>
                            </p>

                            <p>Data inceput:
                                <TimePicker
                                    disabledHours={() => [0, 1, 2, 3, 4, 5, 6, 7, 21, 22, 23, 24]}
                                    value={this.state.calendarSelectedTimeSlotStart}
                                    format={'HH:mm'}
                                    onSelect={(time) => this.onChangeTimePickerValue(time, 'calendarSelectedTimeSlotStart')}
                                />
                            </p>
                            <p>Data sfarsit: <TimePicker
                                disabledHours={() => [0, 1, 2, 3, 4, 5, 6, 7, 21, 22, 23, 24]}
                                value={this.state.calendarSelectedTimeSlotEnd}
                                format={'HH:mm'}
                                onSelect={(time) => this.onChangeTimePickerValue(time, 'calendarSelectedTimeSlotEnd')}
                            />
                                <p style={{fontWeight: "bold", color: "red"}}>
                                    {moment(this.state.calendarSelectedTimeSlotStart).isAfter(moment(this.state.calendarSelectedTimeSlotEnd)) && '*Data de sfarsit nu poate fi mai mica decat data de inceput'}
                                </p>
                            </p>

                            <p>Durata: <b
                                style={{color: moment(this.state.calendarSelectedTimeSlotStart).isAfter(moment(this.state.calendarSelectedTimeSlotEnd)) && "red"}
                                }>{moment(this.state.calendarSelectedTimeSlotEnd).diff(moment(this.state.calendarSelectedTimeSlotStart), 'minutes')} minute</b>
                            </p>
                            {/*<p>Specializare: <b>{this.state.examenDeProgramatInOrar.DenumireSpecializare}</b></p>*/}
                        </Modal.Content>
                        <Modal.Actions>
                            <Button type={'button'} content='Renunță'
                                    onClick={this.handleCloseConfirmareTimeSlot}/>
                            <Button
                                icon='check'
                                color={'green'}
                                content='Programează examen'
                                onClick={this.programeazaExamen}
                            />
                        </Modal.Actions>
                    </Modal>
                </Modal>
                }

                {this.state.existaSesiuniDeProgramat === false ?
                    <Segment style={{textAlign: "center"}}>
                        <h1><Icon name={'calendar times outline'}
                                  style={{transform: "scale(1.4)", marginRight: "20px"}}
                                  color={'red'}/>Momentan nu se pot programa examene.</h1>
                    </Segment> :

                    !this.state.primitDateResponse ?
                        <Segment style={{height: "450px"}}>
                            <Loader size={'big'} active>Se încarcă datele</Loader>
                        </Segment>

                        : this.state.existaSesiuniDeProgramat === true ?
                            <Table selectable>
                                <Table.Header>
                                    <Table.Row className={'colorHeaderCell'}>
                                        <Table.HeaderCell style={{
                                            width: this.state.afisareMateriiTitular && "100px",
                                            backgroundColor: !this.state.afisareMateriiTitular && "#efc75f"
                                        }}>
                                            {this.state.afisareMateriiTitular &&
                                            <Checkbox label='Toate grupele'
                                                      onChange={this.handleCheckbox}
                                                      checked={this.state.checked}/>}
                                        </Table.HeaderCell>
                                        <Table.HeaderCell>Denumire Materie</Table.HeaderCell>
                                        <Table.HeaderCell>Facultate</Table.HeaderCell>
                                        <Table.HeaderCell>Specializare</Table.HeaderCell>
                                        <Table.HeaderCell>An de studiu</Table.HeaderCell>
                                        <Table.HeaderCell>Grupa<br/></Table.HeaderCell>
                                        {/*<Table.HeaderCell>Tip examen</Table.HeaderCell>*/}

                                        <Table.HeaderCell>
                                            {this.state.semestruDinAn === this.state.semestruDinAnSelectat ? "Tip examinare" : null}
                                        </Table.HeaderCell>
                                        <Table.HeaderCell>
                                            {this.state.semestruDinAn === this.state.semestruDinAnSelectat ? "Data si ora" : null}</Table.HeaderCell>
                                        {/*<Table.HeaderCell>Corp</Table.HeaderCell>*/}
                                        <Table.HeaderCell>  {this.state.semestruDinAn === this.state.semestruDinAnSelectat ? "Durata (min)" : null}</Table.HeaderCell>
                                        <Table.HeaderCell>
                                            {this.state.semestruDinAn === this.state.semestruDinAnSelectat ? "Sala sau online" : null}</Table.HeaderCell>
                                        <Table.HeaderCell/>
                                        <Table.HeaderCell/>

                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                    {this.state.afisareMateriiTitular === true ?
                                        this.state.materiiProfesor
                                            .filter((data) =>
                                                this.state.anStudiuSelectat == null
                                                    ? true
                                                    : data.DenumireAnStudiu === this.state.anStudiuSelectat
                                            )
                                            .filter((data) =>
                                                this.state.specializareSelectata == null
                                                    ? true
                                                    : (data.ID_Specializare === this.state.specializareSelectata)
                                            )
                                            .map((element) => {
                                                return (
                                                    <>
                                                        {!this.state.primitDateResponse &&

                                                        <Segment style={{height: "450px"}}
                                                                 key={element.key}>
                                                            <Loader size={'big'} active>Se încarcă
                                                                datele</Loader>
                                                        </Segment>
                                                        }
                                                        <TableRowComponent
                                                            key={element.key}
                                                            style={{clear: "both"}}
                                                            index={element.indexRow}
                                                            activeRow={this.state.activeRow}
                                                            element={element}
                                                            disableRow={this.state.disableRow}
                                                            handleRowDisabled={this.handleRowDisabled.bind(this)}
                                                            cancelDisabledRows={this.cancelDisabledRows.bind(this)}
                                                            handleChangeDate={this.handleChangeDate.bind(this)}
                                                            handleExpandedRows={this.handleExpandedRows.bind(this)}
                                                            programeazaExamen={this.programeazaExamen.bind(this)}
                                                            listaTipExamen={this.state.listaTipExamen}
                                                            listaTipExaminare={this.state.listaTipExaminare}
                                                            esteTableRowSpecializare={true}
                                                            esteTableRowGrupa={false}
                                                            handleTipExaminare={this.handleTipExaminare.bind(this)}
                                                            semestruDinAn={this.state.semestruDinAn}
                                                            sesiuneExamenSelectata={this.state.sesiuneExamenSelectata}
                                                            afisareMateriiTitular={this.state.afisareMateriiTitular}
                                                            listaTipDurataExamen={this.state.listaTipDurataExamen}
                                                            listaSaliFacultate={this.state.listaSaliFacultate}
                                                            handleChangeSala={this.handleChangeSala.bind(this)}
                                                            afisareOrarSali={this.afisareOrarSali.bind(this)}
                                                            isWeekday={isWeekday}
                                                        />

                                                        {
                                                            // (this.state.activeIndex !== -1  && element.Grupe.length>1 ) &&
                                                            (element.isExpanded && element.Grupe && element.Grupe.length > 1) &&
                                                            element.Grupe.map((subelement) => {
                                                                return (
                                                                    <TableRowComponent key={subelement.key}
                                                                                       index={element.indexRow}
                                                                                       activeRow={this.state.activeRow}
                                                                                       element={element}
                                                                                       disableRow={this.state.disableRow}
                                                                                       handleRowDisabled={this.handleRowDisabled.bind(this)}
                                                                                       cancelDisabledRows={this.cancelDisabledRows.bind(this)}
                                                                                       handleChangeDate={this.handleChangeDate.bind(this)}
                                                                                       handleExpandedRows={this.handleExpandedRows.bind(this)}
                                                                                       programeazaExamen={this.programeazaExamen.bind(this)}
                                                                                       listaTipExamen={this.state.listaTipExamen}
                                                                                       listaTipExaminare={this.state.listaTipExaminare}
                                                                                       esteTableRowSpecializare={false}
                                                                                       esteTableRowGrupa={true}
                                                                                       subelement={subelement}
                                                                                       handleTipExaminare={this.handleTipExaminare.bind(this)}
                                                                                       semestruDinAn={this.state.semestruDinAn}
                                                                                       sesiuneExamenSelectata={this.state.sesiuneExamenSelectata}
                                                                                       afisareMateriiTitular={this.state.afisareMateriiTitular}
                                                                                       listaTipDurataExamen={this.state.listaTipDurataExamen}
                                                                                       listaSaliFacultate={this.state.listaSaliFacultate}
                                                                                       handleChangeSala={this.handleChangeSala.bind(this)}
                                                                                       afisareOrarSali={this.afisareOrarSali.bind(this)}
                                                                                       isWeekday={isWeekday}/>
                                                                )
                                                            })}
                                                    </>
                                                )
                                            }) :
                                        this.state.semestruDinAn === this.state.semestruDinAnSelectat && this.state.exameneProfesorAsistent !== undefined &&
                                        this.state.exameneProfesorAsistent.map((examenAsistentCurs, index) => {
                                            return (
                                                <TableRowComponent
                                                    key={examenAsistentCurs.key}
                                                    esteTableAsistentCurs={true}
                                                    index={index}
                                                    activeRow={this.state.activeRow}
                                                    element={examenAsistentCurs}
                                                    disableRow={false}
                                                    esteTableRowSpecializare={false}
                                                    esteTableRowGrupa={false}
                                                    afisareMateriiTitular={this.state.afisareMateriiTitular}
                                                    semestruDinAn={this.state.semestruDinAn}
                                                    sesiuneExamenSelectata={this.state.sesiuneExamenSelectata}/>
                                            )
                                        })
                                    }
                                </Table.Body>
                                {this.state.semestruDinAn === this.state.semestruDinAnSelectat &&
                                <Table.Footer fullWidth>
                                    <Table.Row>
                                        <Table.HeaderCell colSpan='10'>
                                            <Button.Group floated='right' size={"small"}>
                                                <Button
                                                    type={'button'}
                                                    icon
                                                    labelPosition='left'
                                                    color={'blue'}
                                                    onClick={() => this.setState({afisareMateriiTitular: true})}>
                                                    <Icon name='user'/> Materiile mele titular
                                                </Button>
                                                <Button.Or text={'sau'} type={'button'}/>
                                                <Button
                                                    type={'button'}
                                                    style={{backgroundColor: "#efc75f"}}
                                                    icon
                                                    labelPosition='left'
                                                    onClick={() => this.setState({afisareMateriiTitular: false})}>
                                                    <Icon name='calendar alternate outline'
                                                          style={{transform: "scale(1.3)"}}/>Alte materii
                                                    suplinitor
                                                </Button>
                                            </Button.Group>
                                        </Table.HeaderCell>
                                        <Table.HeaderCell/>
                                    </Table.Row>
                                </Table.Footer>
                                }
                            </Table>
                            :
                            <Segment style={{textAlign: "center"}}>
                                <h1><Icon name={'calendar times outline'}
                                          style={{transform: "scale(1.4)", marginRight: "20px"}}
                                          color={'red'}/>Momentan nu
                                    se pot programa examene.</h1>
                            </Segment>}
            </div>
        )
    }
}


export default ProgramareExamene;